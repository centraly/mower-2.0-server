import json
import subprocess
import traceback
from typing import Any, Dict, List
import serial
import time
from loguru import logger


class ArduinoBase:
    def __init__(self, ports: List[str], baud: int):
        self.port = self.identify_correct_port(ports=ports)
        self.baud = baud
        self.serial = serial.Serial(self.port, baud, timeout=1)
        time.sleep(5)
        if self.serial.isOpen():
            logger.info(f"[ArduinoBase]: Successfully connected on port={self.port} at baud={baud}")
        else:
            logger.warning("[ArduinoBase]: Failed to connect on serial")

    @classmethod
    def identify_correct_port(cls, ports: List[str]):
        for port in ports:
            try:
                subprocess.check_output(f"ls {port}*", shell=True)
                return port
            except:
                pass
        logger.critical(f"[ArduinoBase]: No arduino detected on ports: {ports}")
        raise Exception(f"[ArduinoBase]: No arduino detected on ports: {ports}")

    def reconnect(self):
        try:
            self.serial.close()
        finally:
            self.serial = serial.Serial(self.port, self.baud, timeout=1)

    def read_state(self) -> Dict[str, Any]:
        if self.serial.in_waiting > 0:
            line = self.serial.readline()
            line = line.decode().strip().replace("'", '"')
            self.serial.flushInput()
            try:
                return json.loads(line)
            except Exception:
                logger.error(f"[ArduinoBase]: Malformed JSON\n{line}\n{traceback.format_exc()}")
        return {}

    def send_command(self, command: str, value: int):
        if not self.serial.isOpen():
            logger.warning(f"[ArduinoBase]: Serial not open, reconnecting ...")
            self.reconnect()
            time.sleep(5)
        cmd = f"{command}={value}\n".encode()
        logger.debug(f"[ArduinoBase]: Sending command to arduino: {cmd}")
        self.serial.write(cmd)


if __name__ == "__main__":
    ab = ArduinoBase(port="/dev/ttyUSB0", baud=115200)
    ab.send_command(command="left_wheel_motor_forward_set_speed", value=100)

    while 1:
        # ab.send_command(command="left_wheel_motor_forward_set_speed", value=100)
        time.sleep(0.3)
        print(ab.read_state())
