import asyncio
import subprocess
from typing import List

import cv2
from loguru import logger


def identify_correct_port(ports: List[int]):
    for port in ports:
        try:
            subprocess.check_output(f"ls /dev/video{port}*", shell=True)
            return port
        except:
            pass
    logger.critical(f"No cameras detected on ports: {ports}")


port = identify_correct_port(ports=[0, 1])
video = cv2.VideoCapture(port)
video.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
video.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)


class Camera:
    def __init__(self):
        self.video_source = 0

    async def frames(self):
        while True:
            ret, frame = video.read()

            frame_bytes = cv2.imencode(
                ".jpg",
                frame,
                [
                    cv2.IMWRITE_JPEG_QUALITY,
                    50,
                    cv2.IMWRITE_JPEG_PROGRESSIVE,
                    True,
                    cv2.IMWRITE_JPEG_OPTIMIZE,
                    True,
                ],
            )[1].tobytes()
            yield frame_bytes
            await asyncio.sleep(0.01)
