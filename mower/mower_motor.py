"""
Controller class for the mower motor
"""
import RPi.GPIO as GPIO
from mower.board_pin import BoardPin


class MowerMotor:
    def __init__(self, r_en: BoardPin, r_pwm: BoardPin, l_en: BoardPin, l_pwm: BoardPin):
        self.r_en = r_en
        self.r_pwm = r_pwm
        self.l_en = l_en
        self.l_pwm = l_pwm

        GPIO.setup(self.r_en, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.setup(self.r_pwm, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.l_en, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.setup(self.l_pwm, GPIO.OUT, initial=GPIO.LOW)

    def run(self):
        GPIO.output(self.r_pwm, GPIO.HIGH)

    def stop(self):
        GPIO.setup(self.r_pwm, GPIO.OUT, initial=GPIO.LOW)

    def __repr__(self):
        return f"<MowerMotor> (r_en_pin: {self.r_en}, r_pwm_pin: {self.r_pwm}, l_en_pin: {self.l_en}, l_pwm_pin: {self.l_pwm})"

