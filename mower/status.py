import json
from copy import deepcopy
from loguru import logger


class Status:
    def __init__(self, max_size=100):
        self.logs = []
        self.max_size = max_size
        logger.add(lambda x: self.logs.append(x))

    def get_logs(self):
        logs = deepcopy(self.logs[-self.max_size:])
        self.logs = []
        if logs:
            return logs
        return None
