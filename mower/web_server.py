import traceback
import uvicorn
from loguru import logger
from starlette.applications import Starlette
from starlette.endpoints import WebSocketEndpoint
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse
from starlette.routing import Mount, WebSocketRoute
from starlette.websockets import WebSocket
from mower.camera import Camera
# from mower.camera_v2 import Camera
from mower.command_executor import CommandExecutor
from mower.mower_controller import Mower


# @app.route('/stream')
async def stream(scope, receive, send):
    message = await receive()
    camera = Camera()

    if message["type"] == "http.request":
        await send(
            {
                "type": "http.response.start",
                "status": 200,
                "headers": [
                    [b"Content-Type", b"multipart/x-mixed-replace; boundary=frame"]
                ],
            }
        )
        while True:
            try:
                async for frame in camera.frames():
                    data = b"".join(
                        [
                            b"--frame\r\n",
                            b"Content-Type: image/jpeg\r\n\r\n",
                            frame,
                            b"\r\n",
                        ]
                    )
                    await send(
                        {"type": "http.response.body", "body": data, "more_body": True}
                    )
            except Exception as exc:
                print("Client closed", exc)
                return


async def websocket_endpoint(scope, receive, send):
    logger.info("Client connected to websocket!")
    websocket = WebSocket(scope=scope, receive=receive, send=send)
    await websocket.accept()
    while True:
        try:
            payload = await websocket.receive_json()
            logger.debug(f"Submitted payload: {payload}")
            response = command_executor.run(payload=payload)
            if len(response) > 0:
                for item in response:
                    await websocket.send_json(item)
        except Exception:
            logger.error(f"Main loop error: {traceback.format_exc()}")
            logger.info(f"Executing fail safe!")
            command_executor.fail_safe()
            break
    await websocket.close()


# class WebSocketApp(WebSocketEndpoint):
#     encoding = 'bytes'
#
#     async def on_connect(self, websocket):
#         logger.info("Client connected!")
#         await websocket.accept()
#
#     async def on_receive(self, websocket, data):
#         payload = await websocket.receive_json()
#         logger.debug(f"Submitted payload: {payload}")
#         response = command_executor.run(payload=payload)
#         if len(response) > 0:
#             for item in response:
#                 await websocket.send_json(item)
#
#     async def on_disconnect(self, websocket, close_code):
#         logger.info(f"Disconnecting! Executing fail safe!")
#         command_executor.fail_safe()
#

routes = [
    Mount("/stream/", stream),
    Mount("/ws/", websocket_endpoint),
    # WebSocketRoute("/ws/", WebSocketApp),
]

middleware = [
    Middleware(CORSMiddleware, allow_origins=['*'], allow_credentials=True, allow_methods=["*"], allow_headers=["*"]),

]

app = Starlette(debug=False, routes=routes, middleware=middleware)

# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=["*"],  # Allows all origins
#     allow_credentials=True,
#     allow_methods=["*"],  # Allows all methods
#     allow_headers=["*"],  # Allows all headers
# )


@app.route('/heartbeat/')
async def heartbeat(request):
    try:
        # display stats
        mower.display.render()
        # get status data
        status = mower.heartbeat.get_status()
        if status:
            return JSONResponse(status)
        return JSONResponse()
    except Exception:
        logger.error(f"Status loop error: {traceback.format_exc()}")


if __name__ == '__main__':
    mower = Mower(arduino_ports=["/dev/ttyUSB0", "/dev/ttyUSB1"], baud_rate=115200)
    command_executor = CommandExecutor(mower=mower)
    uvicorn.run(app, host='0.0.0.0', port=8000)
