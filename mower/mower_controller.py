from typing import List

from mower.arduino_base import ArduinoBase
from mower.board_pin import BoardPin
from mower.display import Display
from mower.heartbeat import Heartbeat
from mower.mower_motor import MowerMotor
from mower.status import Status


class Mower:
    def __init__(self, arduino_ports: List[str], baud_rate: int):
        self.arduino_base = ArduinoBase(ports=arduino_ports, baud=baud_rate)
        self.status = Status(max_size=100)

        self.heartbeat = Heartbeat(arduino_base=self.arduino_base, status=self.status)

        self.mower_motor = MowerMotor(
            r_en=BoardPin.MOWER_MOTOR_R_EN,
            r_pwm=BoardPin.MOWER_MOTOR_R_PWM,
            l_en=BoardPin.MOWER_MOTOR_L_EN,
            l_pwm=BoardPin.MOWER_MOTOR_L_PWM
        )

        self.gyroscope_sensor = None
        self.ultra_sonic_sensor = None

        # micro OLED display
        self.display = Display(i2c_bus=1)
