from enum import IntEnum
import RPi.GPIO as GPIO
#
GPIO.setmode(GPIO.BOARD)


class BoardPin(IntEnum):

    RIGHT_WHEEL_PWM = 32
    RIGHT_WHEEL_IN1 = 35
    RIGHT_WHEEL_IN2 = 36

    LEFT_WHEEL_PWM = 33
    LEFT_WHEEL_IN1 = 37
    LEFT_WHEEL_IN2 = 38

    MOWER_MOTOR_R_EN = 22
    MOWER_MOTOR_R_PWM = 21
    MOWER_MOTOR_L_EN = 15
    MOWER_MOTOR_L_PWM = 16
