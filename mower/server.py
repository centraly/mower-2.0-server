import asyncio
import traceback
import websockets
from loguru import logger
from command_executor import CommandExecutor
from mower.mower_controller import Mower


async def command_receiver(websocket, path):
    logger.info("Client connected to command_receiver!")
    while 1:
        try:
            payload = eval(await websocket.recv())
            logger.debug(f"Submitted payload: {payload}")
            response = command_executor.run(payload=payload)
            if len(response) > 0:
                for item in response:
                    await websocket.send(item)
        except Exception:
            logger.error(f"Main loop error: {traceback.format_exc()}")
            logger.info(f"Executing fail safe!")
            command_executor.fail_safe()
            break


async def heartbeat(websocket, path):
    logger.info("Client connected to heartbeat!")
    while 1:
        try:
            # get status data
            status = mower.heartbeat.get_status()
            if status:
                await websocket.send(status)
        except Exception:
            logger.error(f"Status loop error: {traceback.format_exc()}")
            break


mower = Mower(arduino_port="/dev/ttyUSB0", baud_rate=115200)
command_executor = CommandExecutor(mower=mower)

command_server = websockets.serve(command_receiver, "0.0.0.0", 8765)
heartbeat_update = websockets.serve(heartbeat, "0.0.0.0", 8766)

asyncio.get_event_loop().run_until_complete(command_server)
asyncio.get_event_loop().run_until_complete(heartbeat_update)
asyncio.get_event_loop().run_forever()
