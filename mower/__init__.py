from pathlib import Path
from loguru import logger

path = Path(__file__).parent.resolve()
log_file = path / "log.txt"

logger.add(log_file, rotation="20 MB")
