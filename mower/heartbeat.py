import json
import time
from mower.arduino_base import ArduinoBase
from mower.status import Status


class Heartbeat:
    def __init__(self, arduino_base: ArduinoBase, status: Status, freq_ms: int = 100):
        self.arduino_base = arduino_base
        self.status = status
        self.freq_ms = freq_ms / 1000.  # get milliseconds
        self.start = time.time()
        self.last_arduino_update = None
        self.arduino_status = None
        self.logs_status = None

    def get_status(self):
        current = time.time()
        # if not enough time has passed, don't report
        if current < self.start + self.freq_ms:
            return None
        arduino_state = self.arduino_base.read_state()
        # if there is any new state use that
        if arduino_state:
            self.arduino_status = arduino_state
            self.last_arduino_update = time.time()

        self.logs_status = self.status.get_logs()

        # update the timer
        self.start = current

        # no update
        if not self.arduino_status and not self.logs_status:
            return None

        # construct the payload
        return json.dumps({
          "arduino": self.arduino_status,
          "logs": self.logs_status
        })
