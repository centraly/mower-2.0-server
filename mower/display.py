from threading import Thread

import Adafruit_SSD1306
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import subprocess
import time


class Display:
    def __init__(self, i2c_bus: int = 1, image_padding: int = -2, fps: int = 5, debug: bool = False):
        self.fps = fps
        # setting gpio to 1 is hack to avoid platform detection
        self.display = Adafruit_SSD1306.SSD1306_128_64(rst=None, i2c_bus=i2c_bus, gpio=1)
        # Initialize library.
        self.display.begin()

        # Clear display.
        self.display.clear()
        self.display.display()

        # display image
        self.image = Image.new('1', (self.display.width, self.display.height))
        self.draw = ImageDraw.Draw(self.image)
        self.top = image_padding
        self.bottom = self.display.height - image_padding

        # Load default font.
        self.font = ImageFont.load_default()
        self.debug = debug
        self.last_call = time.time()

    def clear(self):
        self.draw.rectangle((0, 0, self.display.width, self.display.height), outline=0, fill=0)

    def _get_ip(self):
        if self.debug:
            print("Getting ip!")
        return subprocess.check_output("hostname -I | cut -d\' \' -f1", shell=True)

    def _get_ssid(self):
        if self.debug:
            print("Getting ssid!")
        return subprocess.check_output("iwgetid -r", shell=True)

    def _get_cpu_usage(self):
        if self.debug:
            print("Getting cpu!")
        cmd = "top -bn1 | grep load | awk '{printf \"CPU Load: %.2f\", $(NF-2)}'"
        return subprocess.check_output(cmd, shell=True)

    def _get_memory_usage(self):
        if self.debug:
            print("Getting memory!")
        cmd = "free -m | awk 'NR==2{printf \"Mem: %s/%sMB %.2f%%\", $3,$2,$3*100/$2 }'"
        return subprocess.check_output(cmd, shell=True)

    def _get_disk_usage(self):
        if self.debug:
            print("Getting disk!")
        cmd = "df -h | awk '$NF==\"/\"{printf \"Disk: %d/%dGB %s\", $3,$2,$5}'"
        return subprocess.check_output(cmd, shell=True)

    def _get_date(self):
        if self.debug:
            print("Getting date!")
        return subprocess.check_output("date", shell=True)

    def render(self, vertical_spacing: int = 8):
        if (time.time() - self.last_call) <= 1./self.fps:
            return
        self.clear()
        data = [
            self._get_ssid(),
            self._get_ip(),
            self._get_cpu_usage(),
            self._get_memory_usage(),
            self._get_disk_usage(),
            self._get_date(),
        ]
        y = self.top
        if self.debug:
            print("Drawing")
        for item in data:
            self.draw.text((0, y), str(item.decode("utf-8")), font=self.font, fill=255)
            y += vertical_spacing

        self.display.image(self.image)
        self.display.display()
        self.last_call = time.time()


if __name__ == "__main__":
    disp = Display(debug=True)
    while 1:
        disp.render()
        time.sleep(0.1)
