from typing import Dict, Any, List
from mower.mower_controller import Mower
from loguru import logger
from mower.status import Status


class Command:
    def __init__(self, func: callable, arg_types: List[Any]):
        self.func = func
        self.arg_types = arg_types


class CommandExecutor:
    def __init__(self, mower: Mower):
        self.commands = {
            # left wheel motor
            # "left_wheel_motor_forward_set_speed": Command(func=mower.left_wheel_motor.forward, arg_types=[float]),
            # "left_wheel_motor_backward_set_speed": Command(func=mower.left_wheel_motor.backward, arg_types=[float]),
            # "left_wheel_motor_stop": Command(func=mower.left_wheel_motor.stop, arg_types=[]),
            #
            # # right wheel motor
            # "right_wheel_motor_forward_set_speed": Command(func=mower.right_wheel_motor.forward, arg_types=[float]),
            # "right_wheel_motor_backward_set_speed": Command(func=mower.right_wheel_motor.backward, arg_types=[float]),
            # "right_wheel_motor_stop": Command(func=mower.right_wheel_motor.stop, arg_types=[]),

            # relay commands to arduino
            "arduino": Command(func=mower.arduino_base.send_command, arg_types=[str, int]),

            # mower motor
            "mower_motor_run": Command(func=mower.mower_motor.run, arg_types=[]),
            "mower_motor_stop": Command(func=mower.mower_motor.stop, arg_types=[]),
        }

    def run(self, payload: Dict[str, Any]) -> List[str]:
        res = []
        for key, value in payload.items():
            cmd = self.commands[key]
            logger.debug(f"[CommandExecutor]: Command: {key}  Value: {value}")
            value = [cmd.arg_types[i](val) for i, val in enumerate(value)]
            res.append(cmd.func(*value))
        return list(filter(lambda x: False if x is None else True, res))

    def fail_safe(self):
        self.run(payload={"arduino": ["left_wheel_motor_stop", 0]})
        self.run(payload={"arduino": ["right_wheel_motor_stop", 0]})
        self.run(payload={"mower_motor_stop": []})
