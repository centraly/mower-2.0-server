import asyncio
import cv2
from vidgear.gears import VideoGear

video = CamGear(source=0, stabilize=True, resolution=(224, 224), framerate=15).start()

class Camera:
    def __init__(self):
        self.video_source = 0

    async def frames(self):
        while True:
            frame = video.read()
            frame_bytes = cv2.imencode(
                ".jpg",
                frame,
                [
                    cv2.IMWRITE_JPEG_QUALITY,
                    50,
                    cv2.IMWRITE_JPEG_PROGRESSIVE,
                    True,
                    cv2.IMWRITE_JPEG_OPTIMIZE,
                    True,
                ],
            )[1].tobytes()
            yield frame_bytes
            await asyncio.sleep(0.01)
